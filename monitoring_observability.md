# Monitoring / Observability

## Environments

### Kubernetes Monitoring

- https://github.com/kubernetes/kube-state-metrics 
- https://www.replex.io/blog/kubernetes-in-production-the-ultimate-guide-to-monitoring-resource-metrics
- https://sysdig.com/blog/kubernetes-monitoring-prometheus/

Jaeger Tracing -> Prometheus -> Exemplars in Grafana -> combined with Loki 

#### Hetzner Cloud

https://community.hetzner.com/tutorials/install-kubernetes-cluster
https://www.linhub.de/aufbau-eines-kubernetes-cluster-in-der-hetzner-cloud/
https://www.reddit.com/r/seedboxes/comments/anbdtr/deploy_kubernetes_cluster_to_hetzner_cloud_in_5/


## Tool Ideas

### Prometheus

#### Kubernetes

- Self Discovery https://developer.sh/posts/prometheus-self-discovery

#### TLS 

Exporters need TLS support, node_exporter PR: https://github.com/prometheus/node_exporter/pull/1277 

- Add this to existing exporters in the community?
- Create a skeleton exporter which implements these changes

#### Alerts, Downtimes and Reporting

Whenever a maintenance window is set by admins, alerts should be suppressed. This can be achieved with alert manager rules.
In addition to these suppressed alerts, the downtime maintenance window should be persisted over time.

Rationale: When calculating SLA reports for the service availability, the maintenance downtime window should be removed from
possible alert conditions influencing the calculations.

Alerts themselves are already stored inside the Prometheus TSDB.

##### Existing Addons

- https://promtools.matthiasloibl.com/
- https://github.com/metalmatze/slo-libsonnet 

## Integrations

### Icinga with Prometheus

Evaluate possibilities to integrate Icinga with Prometheus.

- https://github.com/Icinga/icinga2/issues/6419
- https://community.icinga.com/t/icinga-and-prometheus-whats-the-difference/177/2 
