Original: https://github.com/Icinga/icinga2/issues/7462

# Why?

TL;DR - huge Perl scripts fall short with fork a process, start interpreter, compile script, run script, get output.

Instead, using Perl's C API to compile the script into memory once, and cache it for later if not changed on disk. This avoids reading and compiling it again.

Second, when the script is executed, it runs inside the core process with embedded perl instead of an interpreter process itself.

Controlling this should be made a configuration option, and also supporting the old style within the plugin itself.

# PoC

A proof of concept exists in #7461 with a task list.

Below are technical concepts and reasoning how it can be done. This follows best practices known from Icinga 1, Nagios, mod_gearman and centreon.

# Technical Concept


## Embedded Perl

### Initialization

- Init Perl with embed.pl, parse the file and actually run it.
- Populate the m_Interpreter object variable where the method is run against.

```
String embedFile = GetEmbedPath();
char *embedding[] = { const_cast<char *>(""), const_cast<char *>(embedFile.CStr()) };

char *args[] = {
  const_cast<char *>(""),
  const_cast<char *>("0"),
  const_cast<char *>(""),
  const_cast<char *>(""),
  nullptr
};

if ((m_Interpreter = perl_alloc()) == nullptr) {
  Log(LogCritical, "LanguagePerl")
    << "Error: Could not allocate memory for embedded perl interpreter!";
  return 1;
}

perl_construct(m_Interpreter);

int exitState = perl_parse(m_Interpreter, xs_init, 2, embedding, nullptr);

if (!exitState) {
  exitState = perl_run(m_Interpreter);
```

### Enable

Defaults to on.

Can be disabled via CheckCommand attribute or global constant?
Also via plugin header?

#### Parse Plugin Header

Allows to enable/disable embedded perl when defined.

```
#!/bin/perl

# icinga: +epn
# icinga: -epn
```

If this is a Perl plugin, and no header was detected, always use embedded perl?

For compatibility reasons, also support `nagios: +epn`.


### system()

#### Evaluate the perl file

- Add command and arguments on Perl's call stack
- Checks whether it compiles - `call_pv("Embed::Persistent::eval_file", G_EVAL);`
- Needs a pipe to return compilation errors.
- Returns a plugin handler from Perl

```
/**
 * All of those strange functions with sv in their names help convert Perl scalars to C types.
 * https://perldoc.perl.org/perlguts.html#Datatypes
 */

SV* pluginHandlerCheckResult;
STRLEN n_a;
int count = 0;
int pcloseResult;
String output;

/**
 * Parameters are passed to the Perl subroutine using the Perl stack.
 * This is the purpose of the code beginning with the line dSP and ending with the line PUTBACK.
 * The dSP declares a local copy of the stack pointer. This local copy should always be accessed as SP.
 */
dSP; // Parameter stack begin

args[0] = const_cast<char *>(fileName.CStr());
args[2] = const_cast<char *>("");
args[3] = const_cast<char *>(arguments.CStr());

/**
 * Call the Perl interpreter to compile and optionally cache the command.
 *
 * sv_2mortal() creates temporary copies which we'll have to clean later with FREETMPS
 */
ENTER;
SAVETMPS;
PUSHMARK(SP); // Stack pointer.

XPUSHs(sv_2mortal(newSVpv(args[0], 0)));
XPUSHs(sv_2mortal(newSVpv(args[1], 0)));
XPUSHs(sv_2mortal(newSVpv(args[2], 0)));
XPUSHs(sv_2mortal(newSVpv(args[3], 0)));

PUTBACK; // Parameter stack end

/**
 * Evaluate and compile the perl file
 *
 * G_SCALAR: The @_ array will be created and the value returned will still exist after the call to call_pv.
 * G_EVAL: Add an eval trap for die() termination events.
 *
 * https://perldoc.perl.org/perlcall.html
 */
count = call_pv("Embed::Persistent::eval_file", G_SCALAR | G_EVAL);

/**
 * The purpose of the macro SPAGAIN is to refresh the local copy of the stack pointer.
 * This is necessary because it is possible that the memory allocated to the Perl stack
 * has been reallocated during the call_pv call.
 */
SPAGAIN;

/**
 * Check for errors.
 */
if (SvTRUE(ERRSV)) {
  // Pop the return value from the stack and drop it in case of a failure
  (void) POPs;

  Log(LogCritical, "LanguagePerl")
    << "Embedded Perl ran command '" << fileName << " " << arguments << "' " << SvPVX(ERRSV);

  return -2;
}

/**
   * Create a copy of the popped stack result scalar value.
   */
  pluginHandlerCheckResult = newSVsv(POPs);

/**
 * Cleanup the stack
 */
PUTBACK;
FREETMPS;
LEAVE;
```

#### Fork

Create a child process with pipes and everything like "normal".

#### Run with Embedded Perl

- Add real command and arguments onto Perl's stack
- Run the plugin handler

`count = call_pv("Embed::Persistent::run_package", G_ARRAY);`

- POP the results from the stack
- Use a perl wrapper to sanitize the data.

```
/**
 * Run the compiled package plugin stored in pluginHandlerCheckResult.
 */
ENTER;
SAVETMPS;
PUSHMARK(SP); // Opening bracket for arguments on a callback

XPUSHs(sv_2mortal(newSVpv(args[0], 0)));
XPUSHs(sv_2mortal(newSVpv(args[1], 0)));
XPUSHs(pluginHandlerCheckResult);
XPUSHs(sv_2mortal(newSVpv(args[3], 0)));

PUTBACK; // Closing bracket for XSUB arguments.

/*
 * Run it.
 */
count = perl_call_pv("Embed::Persistent::run_package", G_EVAL | G_ARRAY);

/*
 * Refresh the stack pointer copy.
 */
SPAGAIN;

/*
 * Pop the string off the stack (POPpx) as output.
 * Pop the integer off the stack (POPi) as result.
 */
output = POPpx;
pcloseResult = POPi;

Log(LogInformation, "LanguagePerl")
  << "Embedded Perl Plugin return code '" << pcloseResult << "' and output: '" << output << "'.";

/**
 * Cleanup.
 */
PUTBACK;
FREETMPS;
LEAVE;
```

## Perl Packages

### Perl Package: Main

Registers a trap for exit calls from plugins, for further sanitizing after calling `eval()`.

```
package main;

use subs 'CORE::GLOBAL::exit';

sub CORE::GLOBAL::exit { die "ExitTrap: $_[0] (Redefine exit to trap plugin exit with eval BLOCK)" }
```

### Perl Package: OutputTrap

Caches STDOUT in a Perl scalar value.

```
package OutputTrap;

# Methods for use by tied STDOUT in embedded PERL module.
# Simply ties STDOUT to a scalar and caches values written to it.
#
# Note: No more than 4KB characters per line are kept.

sub TIEHANDLE {
	my ($class) = @_;
	my $me = '';
	bless \$me, $class;
}

sub PRINT {
	my $self = shift;

	$$self .= substr(join('',@_), 0, 4096);
}

sub PRINTF {
	my $self = shift;
	my $fmt = shift;

	$$self .= substr(sprintf($fmt,@_), 0, 4096);
}

sub READLINE {
	my $self = shift;
	return $$self;
}

sub CLOSE {
	my $self = shift;
	undef $self ;
}

sub DESTROY {
	my $self = shift;
	undef $self;
}
```

### Perl Package: Embed::Persistent

Defines embedded Perl functions called by the C++ code.

```
package Embed::Persistent;
```

- valid_package_name
- eval_file
- run_package

#### valid_package_name

Ensure that the Perl package name is correct.

```
sub valid_package_name {
	local $_ = shift ;
	s|([^A-Za-z0-9\/])|sprintf("_%2x",unpack("C",$1))|eg;

	# second pass only for words starting with a digit
	s|/(\d)|sprintf("/_%2x",unpack("C",$1))|eg;

	# Dress it up as a real package name
	s|/|::|g;
	return /^::/ ? "Embed$_" : "Embed::$_";
}
```

#### throw_exception

Treat Perl warnings as fatal errors.

```
# Perl 5.005_03 only traps warnings for errors classed by perldiag
# as Fatal (eg 'Global symbol """"%s"""" requires explicit package name').
# Therefore treat all warnings as fatal.

sub throw_exception {
	die shift ;
}
```

#### eval_file

Check whether the plugin is already cached and only update the arguments.

```
my ($filename, $delete, undef, $plugin_args) = @_;

my $mtime = -M $filename;
my $ts = localtime(time()) if DEBUG_LEVEL;

if (exists($Cache{$filename}) &&
  $Cache{$filename}[MTIME] &&
  $Cache{$filename}[MTIME] <= $mtime)
{
  # The plugin was compiled before and it did not
  # change on disk.
  #
  # Only update the following:
  # - Parse the plugin arguments and cache them again, saving repeated parsing.
  #   This ensures that the same plugin works with different command arguments.
  # - Return errors from former compilations if any.

  if ($plugin_args) {
    $Cache{$filename}[PLUGIN_ARGS]{$plugin_args} ||= [ shellwords($plugin_args) ];
  }

  if ($Cache{$filename}[PLUGIN_ERROR]) {
    if (DEBUG_LEVEL & LEAVE_MSG) {
      print LH qq($ts eval_file: $filename failed compilation formerly and file has not changed; skipping compilation.\n);
    }

    die qq(**Embedded Perl failed to compile $filename: "$Cache{$filename}[PLUGIN_ERROR]");
  } else {
    if (DEBUG_LEVEL & LEAVE_MSG) {
      print LH qq($ts eval_file: $filename already successfully compiled and file has not changed; skipping compilation.\n);
    }
    return $Cache{$filename}[PLUGIN_HNDLR];
  }
}
```

Validate the package name from the given filename.

```
my $package = valid_package_name($filename);
```

Cache the plugin arguments.

```
if ($plugin_args) {
  $Cache{$filename}[PLUGIN_ARGS]{$plugin_args} ||= [ shellwords($plugin_args) ];
}
```

Open the perl file.

```
local *FH;

# die must be trapped by caller with ERRSV
open FH, $filename or die qq(**Embedded Perl failed to open "$filename": "$!");

# Create subroutine
my $sub;
sysread FH, $sub, -s FH;
close FH;
```

Wrap the perl plugin's code into a subroutine into our own unique package.

```
# Wrap the code into a subroutine inside our unique package
# (using $_ here [to save a lexical] is not a good idea since
# the definition of the package is visible to any other Perl
# code that uses [non localised] $_).
my $subRoutineHandler = <<EOSUB;
package $package;

sub subRoutineHandler {
\@ARGV = \@_;
local \$^W = 1;

\$ENV{ICINGA_PLUGIN} = '$filename';
# Keep this for compatibility with check_*_health plugins, https://github.com/lausser/check_nwc_health/commit/ec5cc933d72c1255c2df88ef916a211f8cdb6317
\$ENV{NAGIOS_PLUGIN} = '$filename';

# <<< START of PLUGIN (first line of plugin is line 10 in the text) >>>
$sub
# <<< END of PLUGIN >>>
}
EOSUB
```

Update the cached file.

```
$Cache{$filename}[MTIME] = $mtime unless $delete;
```

Trap warnings into an exception.

```
# Suppress warning display.
local $SIG{__WARN__} = \&throw_exception;
```

Enforce caching.

```
# Fix problem where modified Perl plugins didn't get recached by the Embedded Perl Interpreter
no strict 'refs';
undef %{$package.'::'};
use strict 'refs';
```

Evaluate the subRoutineHandler subroutine created earlier.

```
# Compile &$package::subRoutineHandler. Since non executable code is being eval'd
# there is no need to protect lexicals in this scope.
eval $subRoutineHandler;
```

Advanced error handling with debugging messages.

```
# $@ is set for any warning and error.
# This guarantees that the plugin will not be run.
if ($@) {
  # Report error line number to original plugin text (9 lines added by eval_file).
  # Error text looks like
  # 'Use of uninitialized ..' at (eval 23) line 186, <DATA> line 218
  # The error line number is 'line 186'
  chomp($@);
  $@ =~ s/line (\d+)[\.,]/'line ' . ($1 - 9) . ','/e;

  if (DEBUG_LEVEL & LEAVE_MSG) {
    print LH qq($ts eval_file: syntax error in $filename: "$@".\n);
  }

  if (DEBUG_LEVEL & PLUGIN_DUMP) {
    my $i = 1;
    $_ = $subRoutineHandler;
    s/^/sprintf('%10d  ', $i++)/meg ;
    # Will only get here once (when a faulty plugin is compiled).
    # Therefore only _faulty_ plugins are dumped once each time the text changes.

    print PH qq($ts eval_file: transformed plugin "$filename" to ==>\n$_\n);
  }

  if (length($@) > 4096) {
    $@ = substr($@, 0, 4096);
  }

  $Cache{$filename}[PLUGIN_ERROR] = $@;

  # If the compilation fails, leave nothing behind that may affect subsequent
  # compilations. This must be trapped by caller with ERRSV.
  die qq(**Embedded Perl failed to compile $filename: "$@");

} else {
  $Cache{$filename}[PLUGIN_ERROR] = '';
}
```

Cache the successful Perl code compilation handler for the actual run.

```
no strict 'refs';
return $Cache{$filename}[PLUGIN_HNDLR] = *{ $package . '::hndlr' }{CODE};
```

#### run_package

Works similar to `eval_file` with the preparations, e.g. with treating warnings as fatal errors,
handlers for stdout and reading the cached plugin arguments.

```
# Treat Perl warnings as fatal error.
local $SIG{__WARN__} = \&throw_exception;

# Tie stdout to sanitize handlers
my $stdout = tie (*STDOUT, 'OutputTrap');

# Read cached plugin arguments
my @plugin_args	= $plugin_args ? @{ $Cache{$filename}[PLUGIN_ARGS]{$plugin_args} } : ();
```

Run the handler with arguments. `eval_file` has updated them before.

```
# If the plugin has args, they have been cached by eval_file.
# (cannot cache @plugin_args here because run_package() is
# called by child processes so cannot update %Cache.)

eval { $plugin_hndlr_cr->(@plugin_args) };
```

Error handling and checks whether exit() was called correctly by plugin developers.

Read the plugin output from stdout and return the output and the state.

```
# !! (read any output from the tied file handle.)
my $plugin_output = <STDOUT>;

undef $stdout;
untie *STDOUT;

if ($has_exit == 0) {
  $plugin_output = "**Embedded Perl $filename: plugin did not call exit()\n" . $plugin_output;
}

if (DEBUG_LEVEL & LEAVE_MSG) {
  print LH qq($ts run_package: "$filename $plugin_args" returning ($res, "$plugin_output").\n);
}

return ($res, $plugin_output);
```

### Sanitize Data

Use embed.pl (former p1.pl) to sanitize the returned plugin data.



### Cache compiled plugin

`call_pv("Embed::Persistent::eval_file", G_EVAL);`

### Multiple interpreters

- Perl's thread safety needs to be instrumented.

## Resources

### Perl Documentation

https://perldoc.perl.org/perlembed.html

https://perldoc.perl.org/perlguts.html#Datatypes
https://perldoc.perl.org/perlxs.html#The-Anatomy-of-an-XSUB

https://perldoc.perl.org/perlapi.html#Callback-Functions

https://icinga.com/docs/icinga1/latest/en/epnplugins.html

### Implementation References

https://github.com/Leont/libperl--

https://github.com/Icinga/icinga-core/blob/master/contrib/mini_epn.c
https://github.com/Icinga/icinga-core/blob/master/base/utils.c#L3646

https://github.com/sni/mod_gearman/blob/master/common/epn_utils.c

https://github.com/irssi/irssi/blob/master/src/perl/perl-core.c

https://git.octo.it/?p=collectd.git;a=blob;f=src/perl.c;h=78e508ae4d1f1bb7c0b9ae5c9ca340b9685e79e6;hb=master

### Others

https://warwick.ac.uk/fac/sci/moac/people/students/2007/nigel_dyer/phd/software/callingperlfromcpp/
http://notebook.kulchenko.com/modeling/practice/embedding-perl-in-cpp
https://www.codeproject.com/Articles/3037/CPerlWrap-A-Class-Wrapper-for-embedding-Perl-into

https://stackoverflow.com/questions/12403729/c-calling-perl-code-eval-sv-not-passing-arguments-to-script
https://stackoverflow.com/questions/45896411/perl-newxs-with-closure-added/45897500

