# Recurring

- [My Issues](https://gitlab.com/dashboard/issues?assignee_username=dnsmichi)

# Calendar Week 28 (2020-07-06)

## Focus

- CI Security Webcast
- Blogpost on GitChallenge
- Workflow improvements
  - Dedicated OSS day

## Content

- [x] Blogposts & howtos
  - [x] [Git Challenge](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/51735)
  - [x] Review changes done
- [x] TE Handbook: Social https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/2227 

## Events

### Collision from Home

- [x] Feedback & Retro

### CI Security Webcast

https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/2042

- [x] Demo
- [x] Review demo & slides (Tue)
- [x] Dry run (Tue)

### Community

- [x] `#everyonecancontribute Kaeffchen` (Wed)
  - https://gitlab.com/everyonecancontribute/general/-/issues/18

### Meetings

- [x] Weekly TE (Mon)
- [x] Weekly CR (Tue)
- [x] 1:1 David (Wed)
- [x] 1:1 Brendan (Thu)
- [x] Ops Cross Stage Think Big (Thu)
- [x] Marketing (Thu)
- [x] CR any topic (Fri)

### ☕ chat

- [x] Gabriel (Mon)
- [x] Veethika (Wed)
