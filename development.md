# GitLab Development

## Environment on macOS Catalina

Follow the documentation:

- [Prepare](https://gitlab.com/dnsmichi/gitlab-development-kit/blob/master/doc/prepare.md#macos)
- [Setup GDK](https://gitlab.com/dnsmichi/gitlab-development-kit/blob/master/doc/set-up-gdk.md)

### RVM 

Extract the GitLab Ruby version from https://gitlab.com/gitlab-org/gitlab/blob/master/.ruby-version and install it with rvm.

```
RUBY_VERSION=$(curl -Ls https://gitlab.com/gitlab-org/gitlab/raw/master/.ruby-version)
rvm install $RUBY_VERSION
```

### GDK

```
rvm use $RUBY_VERSION

gem install gitlab-development-kit

cd $HOME/dev/gitlab

gdk init

cd gitlab-development-kit


```

### Own GitLab Fork

> TODO: Upstream MR, missing dependencies.

```
gem install bundler
```

```
gdk install gitlab_repo=https://gitlab.com/dnsmichi/gitlab.git
```

Fails. Read the Makefile and extract the steps for gitlab-bundle.


```
cd gitlab 
bundle install --jobs 4 --without production
```

https://gitlab.com/gitlab-org/gitlab-development-kit/issues/618
-> https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc%2Fhowto%2Ftroubleshooting.md#gem-install-gpgme-20x-fails-to-compile-native-extension-on-macos-mojave 

```
brew install gpgme
gem install gpgme -- --use-system-libraries
bundle config build.gpgme --use-system-libraries
```

```
gdk install gitlab_repo=https://gitlab.com/dnsmichi/gitlab.git
support/set-gitlab-upstream
```

```
gdk start
```

http://localhost:3000 root/5iveL!fe

Wait a bit til the socket error is gone.


### EE

Only for employees.
